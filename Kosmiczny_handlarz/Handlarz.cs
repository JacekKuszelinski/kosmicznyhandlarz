﻿/*
 *\file Sklep.cs
 *\author Jacek Kuszeliński, Kevin Łebko
 *\date 24.06.2018
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kosmiczny_handlarz
{
    /// <summary>
    /// Klasa tworząca gracza tzn. Handlarza
    /// </summary>
    public class Handlarz 
    {
        #region Deklaracje

        string nazwa; // Tekst
        int paliwo, zywnosc, zaloga, kadlub, portfel, runda; // Liczby
        int radioaktivum, krysztal, deuter, antymateria;

        #endregion

        #region Inicjalizacja
        /// <summary>
        /// Konstruktor Handlarza 
        /// </summary>
        /// <param name="_nazwa"> Nazwa kapitana statku </param>
        public Handlarz(string _nazwa) 
        {
            // Ustawienie wstępnych zasobów Handlarza podczas jego stworzenia
            nazwa = _nazwa;
            paliwo = 400;
            zywnosc = 20;
            zaloga = 5;
            kadlub = 100;
            portfel = 1000;
            runda = 0;

            radioaktivum = 1;
            krysztal = 2;
            deuter = 1;
            antymateria = 0;
       }

        #endregion

        // Getery i Setery
        #region Wlasciwosci 


        public int Radioaktivum
        {
            set { radioaktivum = value; }
            get { return radioaktivum; }
        }
        public int Krysztal
        {
            set { krysztal = value; }
            get { return krysztal; }
        }
        public int Deuter
        {
            set { deuter = value; }
            get { return deuter; }
        }
        public int Antymateria
        {
            set { antymateria = value; }
            get { return antymateria; }
        }
        public int Runda
        {
            set { runda = value; }
            get { return runda; }
        }
        public string Nazwa
        {
            set { nazwa = value; }
            get { return nazwa; }
        }
        public int Paliwo
        {
            set { paliwo = value; }
            get { return paliwo; }
        }
        public int Zywnosc
        {
            set { zywnosc = value; }
            get { return zywnosc; }
        }
        public int Zaloga
        {
            set { zaloga = value; }
            get { return zaloga; }
        }
        public int Kadlub
        {
            set { kadlub = value; }
            get { return kadlub; }
        }
        public int Portfel
        {
            set { portfel = value; }
            get { return portfel; }
        }

        #endregion

    }
}

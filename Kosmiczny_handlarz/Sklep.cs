﻿/*
 *\file Sklep.cs
 *\author Jacek Kuszeliński, Kevin Łebko
 *\date 24.06.2018
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kosmiczny_handlarz
{
    /// <summary>
    /// Klasa sklep : ceny produktów
    /// </summary>
    public class Sklep
    {
        #region Deklaracje

        // Zmienna losowa
        static Random random = new Random();
        // Zmienne
        int paliwo_cena, zywnosc_cena, kadlub_cena, zaloga_cena;
        int radioaktivum_cena, krysztal_cena, deuter_cena, antymateria_cena;
        #endregion

        #region Inicjalizacja
        /// <summary>
        /// Stworzenie sklepu - konstruktor
        /// Każdy sklep ma losowe ceny produktów
        /// </summary>
        public Sklep() 
        {
            paliwo_cena = random.Next(100, 200);
            zywnosc_cena = random.Next(25, 60);
            kadlub_cena = 100;
            zaloga_cena = random.Next(100, 500);

            radioaktivum_cena = random.Next(600, 1000);
            krysztal_cena = random.Next(200, 600);
            deuter_cena = random.Next(100, 300);
            antymateria_cena = random.Next(2000, 3000);
        }

        #endregion

        // Getery i Setery
        #region Właściwości

        public int Paliwo
        {
            get { return paliwo_cena; }
            set { paliwo_cena = value; }
        }
        public int Zywnosc
        {
            get { return zywnosc_cena; }
            set { zywnosc_cena = value; }
        }
        public int Kadlub
        {
            get { return kadlub_cena; }
            set { kadlub_cena = value; }
        }
        public int Zaloga
        {
            get { return zaloga_cena; }
            set { zaloga_cena = value; }
        }

        public int Radioaktivum_cena
        {
            get { return radioaktivum_cena; }
            set { radioaktivum_cena = value; }
        }
        public int Krysztal_cena
        {
            get { return krysztal_cena; }
            set { krysztal_cena = value; }
        }
        public int Deuter_cena
        {
            get { return deuter_cena; }
            set { deuter_cena = value; }
        }
        public int Antymateria_cena
        {
            get { return antymateria_cena; }
            set { antymateria_cena = value; }
        }
        #endregion
    }
}

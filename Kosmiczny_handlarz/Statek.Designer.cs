﻿namespace Kosmiczny_handlarz
{
    partial class Statek
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_stacja = new System.Windows.Forms.Label();
            this.lbl_paliwo = new System.Windows.Forms.Label();
            this.lbl_kapitan = new System.Windows.Forms.Label();
            this.lbl_zywnosc = new System.Windows.Forms.Label();
            this.lbl_zaloga = new System.Windows.Forms.Label();
            this.lbl_kadlub = new System.Windows.Forms.Label();
            this.lbl_portfel = new System.Windows.Forms.Label();
            this.lbl_wyposazenie = new System.Windows.Forms.Label();
            this.groupbox_sklep = new System.Windows.Forms.GroupBox();
            this.rb_antymateria = new System.Windows.Forms.RadioButton();
            this.rb_deuter = new System.Windows.Forms.RadioButton();
            this.rb_krysztal = new System.Windows.Forms.RadioButton();
            this.rb_radioaktivum = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.rb_kadlub = new System.Windows.Forms.RadioButton();
            this.rb_zaloga = new System.Windows.Forms.RadioButton();
            this.rb_zywnosc = new System.Windows.Forms.RadioButton();
            this.rb_paliwo = new System.Windows.Forms.RadioButton();
            this.button_kup = new System.Windows.Forms.Button();
            this.button_runda = new System.Windows.Forms.Button();
            this.lbl_runda = new System.Windows.Forms.Label();
            this.lbl_radioaktivum = new System.Windows.Forms.Label();
            this.lbl_krysztal = new System.Windows.Forms.Label();
            this.lbl_deuter = new System.Windows.Forms.Label();
            this.lbl_antymateria = new System.Windows.Forms.Label();
            this.button_sprzedaj = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupbox_sklep.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_stacja
            // 
            this.lbl_stacja.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_stacja.Location = new System.Drawing.Point(200, 9);
            this.lbl_stacja.Name = "lbl_stacja";
            this.lbl_stacja.Size = new System.Drawing.Size(248, 39);
            this.lbl_stacja.TabIndex = 0;
            this.lbl_stacja.Text = "Kosmiczna Stacja";
            this.lbl_stacja.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_paliwo
            // 
            this.lbl_paliwo.AutoSize = true;
            this.lbl_paliwo.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_paliwo.Location = new System.Drawing.Point(12, 122);
            this.lbl_paliwo.Name = "lbl_paliwo";
            this.lbl_paliwo.Size = new System.Drawing.Size(61, 18);
            this.lbl_paliwo.TabIndex = 3;
            this.lbl_paliwo.Text = "Paliwo : ";
            // 
            // lbl_kapitan
            // 
            this.lbl_kapitan.AutoSize = true;
            this.lbl_kapitan.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_kapitan.Location = new System.Drawing.Point(14, 51);
            this.lbl_kapitan.Name = "lbl_kapitan";
            this.lbl_kapitan.Size = new System.Drawing.Size(117, 18);
            this.lbl_kapitan.TabIndex = 4;
            this.lbl_kapitan.Text = "Kapitan Statku : ";
            // 
            // lbl_zywnosc
            // 
            this.lbl_zywnosc.AutoSize = true;
            this.lbl_zywnosc.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_zywnosc.Location = new System.Drawing.Point(12, 142);
            this.lbl_zywnosc.Name = "lbl_zywnosc";
            this.lbl_zywnosc.Size = new System.Drawing.Size(78, 18);
            this.lbl_zywnosc.TabIndex = 5;
            this.lbl_zywnosc.Text = "Zywnosc : ";
            // 
            // lbl_zaloga
            // 
            this.lbl_zaloga.AutoSize = true;
            this.lbl_zaloga.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_zaloga.Location = new System.Drawing.Point(12, 165);
            this.lbl_zaloga.Name = "lbl_zaloga";
            this.lbl_zaloga.Size = new System.Drawing.Size(64, 18);
            this.lbl_zaloga.TabIndex = 6;
            this.lbl_zaloga.Text = "Zaloga : ";
            // 
            // lbl_kadlub
            // 
            this.lbl_kadlub.AutoSize = true;
            this.lbl_kadlub.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_kadlub.Location = new System.Drawing.Point(12, 185);
            this.lbl_kadlub.Name = "lbl_kadlub";
            this.lbl_kadlub.Size = new System.Drawing.Size(102, 18);
            this.lbl_kadlub.TabIndex = 7;
            this.lbl_kadlub.Text = "Stan kadłuba : ";
            // 
            // lbl_portfel
            // 
            this.lbl_portfel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_portfel.Location = new System.Drawing.Point(12, 415);
            this.lbl_portfel.Name = "lbl_portfel";
            this.lbl_portfel.Size = new System.Drawing.Size(276, 57);
            this.lbl_portfel.TabIndex = 9;
            this.lbl_portfel.Text = "Ilość pieniędzy : ";
            this.lbl_portfel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_wyposazenie
            // 
            this.lbl_wyposazenie.AutoSize = true;
            this.lbl_wyposazenie.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_wyposazenie.Location = new System.Drawing.Point(9, 101);
            this.lbl_wyposazenie.Name = "lbl_wyposazenie";
            this.lbl_wyposazenie.Size = new System.Drawing.Size(105, 18);
            this.lbl_wyposazenie.TabIndex = 10;
            this.lbl_wyposazenie.Text = "Twoje zasoby : ";
            // 
            // groupbox_sklep
            // 
            this.groupbox_sklep.Controls.Add(this.rb_antymateria);
            this.groupbox_sklep.Controls.Add(this.rb_deuter);
            this.groupbox_sklep.Controls.Add(this.rb_krysztal);
            this.groupbox_sklep.Controls.Add(this.rb_radioaktivum);
            this.groupbox_sklep.Controls.Add(this.label1);
            this.groupbox_sklep.Controls.Add(this.rb_kadlub);
            this.groupbox_sklep.Controls.Add(this.rb_zaloga);
            this.groupbox_sklep.Controls.Add(this.rb_zywnosc);
            this.groupbox_sklep.Controls.Add(this.rb_paliwo);
            this.groupbox_sklep.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupbox_sklep.Location = new System.Drawing.Point(326, 51);
            this.groupbox_sklep.Name = "groupbox_sklep";
            this.groupbox_sklep.Size = new System.Drawing.Size(272, 289);
            this.groupbox_sklep.TabIndex = 11;
            this.groupbox_sklep.TabStop = false;
            this.groupbox_sklep.Text = "Sklep";
            // 
            // rb_antymateria
            // 
            this.rb_antymateria.AutoSize = true;
            this.rb_antymateria.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_antymateria.Location = new System.Drawing.Point(23, 252);
            this.rb_antymateria.Name = "rb_antymateria";
            this.rb_antymateria.Size = new System.Drawing.Size(108, 21);
            this.rb_antymateria.TabIndex = 8;
            this.rb_antymateria.TabStop = true;
            this.rb_antymateria.Text = "Antymateria ";
            this.rb_antymateria.UseVisualStyleBackColor = true;
            // 
            // rb_deuter
            // 
            this.rb_deuter.AutoSize = true;
            this.rb_deuter.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_deuter.Location = new System.Drawing.Point(23, 224);
            this.rb_deuter.Name = "rb_deuter";
            this.rb_deuter.Size = new System.Drawing.Size(69, 21);
            this.rb_deuter.TabIndex = 7;
            this.rb_deuter.TabStop = true;
            this.rb_deuter.Text = "Deuter";
            this.rb_deuter.UseVisualStyleBackColor = true;
            // 
            // rb_krysztal
            // 
            this.rb_krysztal.AutoSize = true;
            this.rb_krysztal.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_krysztal.Location = new System.Drawing.Point(23, 196);
            this.rb_krysztal.Name = "rb_krysztal";
            this.rb_krysztal.Size = new System.Drawing.Size(73, 21);
            this.rb_krysztal.TabIndex = 6;
            this.rb_krysztal.TabStop = true;
            this.rb_krysztal.Text = "Kryszał";
            this.rb_krysztal.UseVisualStyleBackColor = true;
            // 
            // rb_radioaktivum
            // 
            this.rb_radioaktivum.AutoSize = true;
            this.rb_radioaktivum.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_radioaktivum.Location = new System.Drawing.Point(24, 168);
            this.rb_radioaktivum.Name = "rb_radioaktivum";
            this.rb_radioaktivum.Size = new System.Drawing.Size(118, 21);
            this.rb_radioaktivum.TabIndex = 5;
            this.rb_radioaktivum.TabStop = true;
            this.rb_radioaktivum.Text = "RadioAktivum ";
            this.rb_radioaktivum.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 24);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kup/Sprzedaj";
            // 
            // rb_kadlub
            // 
            this.rb_kadlub.AutoSize = true;
            this.rb_kadlub.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_kadlub.Location = new System.Drawing.Point(24, 114);
            this.rb_kadlub.Name = "rb_kadlub";
            this.rb_kadlub.Size = new System.Drawing.Size(128, 21);
            this.rb_kadlub.TabIndex = 3;
            this.rb_kadlub.TabStop = true;
            this.rb_kadlub.Text = "Napraw statek : ";
            this.rb_kadlub.UseVisualStyleBackColor = true;
            // 
            // rb_zaloga
            // 
            this.rb_zaloga.AutoSize = true;
            this.rb_zaloga.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_zaloga.Location = new System.Drawing.Point(24, 86);
            this.rb_zaloga.Name = "rb_zaloga";
            this.rb_zaloga.Size = new System.Drawing.Size(170, 21);
            this.rb_zaloga.TabIndex = 2;
            this.rb_zaloga.TabStop = true;
            this.rb_zaloga.Text = "Dokup członka załogi : ";
            this.rb_zaloga.UseVisualStyleBackColor = true;
            // 
            // rb_zywnosc
            // 
            this.rb_zywnosc.AutoSize = true;
            this.rb_zywnosc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rb_zywnosc.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_zywnosc.Location = new System.Drawing.Point(24, 58);
            this.rb_zywnosc.Name = "rb_zywnosc";
            this.rb_zywnosc.Size = new System.Drawing.Size(90, 21);
            this.rb_zywnosc.TabIndex = 1;
            this.rb_zywnosc.TabStop = true;
            this.rb_zywnosc.Text = "Żywność : ";
            this.rb_zywnosc.UseVisualStyleBackColor = true;
            // 
            // rb_paliwo
            // 
            this.rb_paliwo.AutoSize = true;
            this.rb_paliwo.Font = new System.Drawing.Font("Caladea", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rb_paliwo.Location = new System.Drawing.Point(24, 30);
            this.rb_paliwo.Name = "rb_paliwo";
            this.rb_paliwo.Size = new System.Drawing.Size(69, 21);
            this.rb_paliwo.TabIndex = 0;
            this.rb_paliwo.TabStop = true;
            this.rb_paliwo.Text = "Paliwo";
            this.rb_paliwo.UseVisualStyleBackColor = true;
            // 
            // button_kup
            // 
            this.button_kup.Font = new System.Drawing.Font("Britannic Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_kup.Location = new System.Drawing.Point(326, 346);
            this.button_kup.Name = "button_kup";
            this.button_kup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_kup.Size = new System.Drawing.Size(130, 50);
            this.button_kup.TabIndex = 12;
            this.button_kup.Text = "Kup";
            this.button_kup.UseVisualStyleBackColor = true;
            this.button_kup.Click += new System.EventHandler(this.button_kup_Click);
            // 
            // button_runda
            // 
            this.button_runda.Font = new System.Drawing.Font("Britannic Bold", 20.25F);
            this.button_runda.Location = new System.Drawing.Point(293, 418);
            this.button_runda.Name = "button_runda";
            this.button_runda.Size = new System.Drawing.Size(305, 54);
            this.button_runda.TabIndex = 13;
            this.button_runda.Text = "Leć do kolejnej stacji";
            this.button_runda.UseVisualStyleBackColor = true;
            this.button_runda.Click += new System.EventHandler(this.button_runda_Click);
            // 
            // lbl_runda
            // 
            this.lbl_runda.AutoSize = true;
            this.lbl_runda.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_runda.Location = new System.Drawing.Point(464, 19);
            this.lbl_runda.Name = "lbl_runda";
            this.lbl_runda.Size = new System.Drawing.Size(65, 19);
            this.lbl_runda.TabIndex = 14;
            this.lbl_runda.Text = "Runda : ";
            // 
            // lbl_radioaktivum
            // 
            this.lbl_radioaktivum.AutoSize = true;
            this.lbl_radioaktivum.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_radioaktivum.Location = new System.Drawing.Point(12, 217);
            this.lbl_radioaktivum.Name = "lbl_radioaktivum";
            this.lbl_radioaktivum.Size = new System.Drawing.Size(116, 18);
            this.lbl_radioaktivum.TabIndex = 15;
            this.lbl_radioaktivum.Text = "Radio Aktivum : ";
            // 
            // lbl_krysztal
            // 
            this.lbl_krysztal.AutoSize = true;
            this.lbl_krysztal.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_krysztal.Location = new System.Drawing.Point(12, 237);
            this.lbl_krysztal.Name = "lbl_krysztal";
            this.lbl_krysztal.Size = new System.Drawing.Size(73, 18);
            this.lbl_krysztal.TabIndex = 16;
            this.lbl_krysztal.Text = "Kryształ : ";
            // 
            // lbl_deuter
            // 
            this.lbl_deuter.AutoSize = true;
            this.lbl_deuter.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_deuter.Location = new System.Drawing.Point(12, 254);
            this.lbl_deuter.Name = "lbl_deuter";
            this.lbl_deuter.Size = new System.Drawing.Size(66, 18);
            this.lbl_deuter.TabIndex = 17;
            this.lbl_deuter.Text = "Deuter : ";
            // 
            // lbl_antymateria
            // 
            this.lbl_antymateria.AutoSize = true;
            this.lbl_antymateria.Font = new System.Drawing.Font("Kristen ITC", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_antymateria.Location = new System.Drawing.Point(12, 274);
            this.lbl_antymateria.Name = "lbl_antymateria";
            this.lbl_antymateria.Size = new System.Drawing.Size(100, 18);
            this.lbl_antymateria.TabIndex = 18;
            this.lbl_antymateria.Text = "Antymateria : ";
            // 
            // button_sprzedaj
            // 
            this.button_sprzedaj.Font = new System.Drawing.Font("Britannic Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_sprzedaj.Location = new System.Drawing.Point(468, 346);
            this.button_sprzedaj.Name = "button_sprzedaj";
            this.button_sprzedaj.Size = new System.Drawing.Size(130, 50);
            this.button_sprzedaj.TabIndex = 19;
            this.button_sprzedaj.Text = "Sprzedaj";
            this.button_sprzedaj.UseVisualStyleBackColor = true;
            this.button_sprzedaj.Click += new System.EventHandler(this.button_sprzedaj_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(273, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 15);
            this.label2.TabIndex = 20;
            this.label2.Text = "100-200";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(285, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 15);
            this.label3.TabIndex = 21;
            this.label3.Text = "25-60";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(273, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 15);
            this.label4.TabIndex = 22;
            this.label4.Text = "100-500";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(295, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 15);
            this.label5.TabIndex = 23;
            this.label5.Text = "100";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(267, 224);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 15);
            this.label6.TabIndex = 24;
            this.label6.Text = "600-1000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(273, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 15);
            this.label7.TabIndex = 25;
            this.label7.Text = "200-600";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(273, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 15);
            this.label8.TabIndex = 26;
            this.label8.Text = "100-300";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(261, 308);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 15);
            this.label9.TabIndex = 27;
            this.label9.Text = "2000-3000";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(223, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Ceny w sklepach : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Kristen ITC", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(163, 23);
            this.label11.TabIndex = 29;
            this.label11.Text = "Kapitan Statku : ";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Statek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(610, 484);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button_sprzedaj);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_antymateria);
            this.Controls.Add(this.lbl_deuter);
            this.Controls.Add(this.lbl_krysztal);
            this.Controls.Add(this.lbl_radioaktivum);
            this.Controls.Add(this.lbl_runda);
            this.Controls.Add(this.button_runda);
            this.Controls.Add(this.button_kup);
            this.Controls.Add(this.groupbox_sklep);
            this.Controls.Add(this.lbl_wyposazenie);
            this.Controls.Add(this.lbl_portfel);
            this.Controls.Add(this.lbl_kadlub);
            this.Controls.Add(this.lbl_zaloga);
            this.Controls.Add(this.lbl_zywnosc);
            this.Controls.Add(this.lbl_kapitan);
            this.Controls.Add(this.lbl_paliwo);
            this.Controls.Add(this.lbl_stacja);
            this.Name = "Statek";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Statek_Load);
            this.groupbox_sklep.ResumeLayout(false);
            this.groupbox_sklep.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_stacja;
        private System.Windows.Forms.Label lbl_paliwo;
        private System.Windows.Forms.Label lbl_kapitan;
        private System.Windows.Forms.Label lbl_zywnosc;
        private System.Windows.Forms.Label lbl_zaloga;
        private System.Windows.Forms.Label lbl_kadlub;
        private System.Windows.Forms.Label lbl_portfel;
        private System.Windows.Forms.Label lbl_wyposazenie;
        private System.Windows.Forms.GroupBox groupbox_sklep;
        private System.Windows.Forms.RadioButton rb_paliwo;
        private System.Windows.Forms.RadioButton rb_kadlub;
        private System.Windows.Forms.RadioButton rb_zaloga;
        private System.Windows.Forms.RadioButton rb_zywnosc;
        private System.Windows.Forms.Button button_kup;
        private System.Windows.Forms.Button button_runda;
        private System.Windows.Forms.Label lbl_runda;
        private System.Windows.Forms.RadioButton rb_antymateria;
        private System.Windows.Forms.RadioButton rb_deuter;
        private System.Windows.Forms.RadioButton rb_krysztal;
        private System.Windows.Forms.RadioButton rb_radioaktivum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_radioaktivum;
        private System.Windows.Forms.Label lbl_krysztal;
        private System.Windows.Forms.Label lbl_deuter;
        private System.Windows.Forms.Label lbl_antymateria;
        private System.Windows.Forms.Button button_sprzedaj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}


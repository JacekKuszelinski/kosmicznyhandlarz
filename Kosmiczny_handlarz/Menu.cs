﻿/*
 *\file Sklep.cs
 *\author Jacek Kuszeliński
 *\date 24.06.2018
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kosmiczny_handlarz
{
    /// <summary>
    /// Formularz wstępny do pobrania nazwy gracza
    /// </summary>
    public partial class Menu : Form
    {
        // Deklaracja
        public string nazwa_gracza;


        // Inicjalizacja 
        public Menu()
        {
            InitializeComponent();
        }

        // Metoda prywatna
        private void button1_Click(object sender, EventArgs e)
        {
            // Warunek jeżeli pole tekstowe jest puste albo jest spacją
            if (string.IsNullOrWhiteSpace(tb_nazwa.Text) && (string.IsNullOrWhiteSpace(tb_nazwa.Text)))
            {
                string w1 = "Wprowadź nazwę gracza";
                string w2 = "Błąd!!!";
                MessageBoxButtons przycisk = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(w1, w2, przycisk);
            }
            else
            {
                //Pobiera TextBox i przypisuje do zmiennej nazwa_gracza
                this.Nazwa_gracza = tb_nazwa.Text;

                //Zamyka formularz
                this.Close();
            }
            
        }
        //Właściwość
        public string Nazwa_gracza
        {
            set { nazwa_gracza = value; }
            get { return nazwa_gracza; }
        }
    }
}
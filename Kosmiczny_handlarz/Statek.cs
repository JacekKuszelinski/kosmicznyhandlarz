﻿/*
 *\file Statek.cs
 *\author Jacek Kuszeliński, Kevin Łebko
 *\date 24.06.2018
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Kosmiczny_handlarz
{
    /// <summary>
    /// Windows form Statek / stół do gry
    /// </summary>
    public partial class Statek : Form
    {
        #region Deklaracje
        // Zadeklarowanie zmiennych 
        Handlarz handlarz; // Obiekt handlarz z Klasy Handlarz
        Sklep sklep; // Obiekt sklep z Klasy Sklep
        static Random rnd = new Random(); // Zmienna losowa.
        #endregion

        #region Inicjalizacja

        // Domyślne
        public Statek()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Aktualizacja kontrolek na formie
        /// Po np. zakupie produktu aktualizowana jest etykieta z posiadaną ilością przez handlarza.
        /// </summary>
        public void Aktualizuj_handlarza()
        {
            // Nazwa kapitana statku + łączenie tekstu "..." etykiety i pobranie wartości ze zmiennej
            lbl_kapitan.Text = handlarz.Nazwa;
            lbl_portfel.Text = "Ilość pieniędzy : " + handlarz.Portfel;
            lbl_paliwo.Text = "Paliwo : " + handlarz.Paliwo;
            lbl_zywnosc.Text = "Żywność : " + handlarz.Zywnosc;
            lbl_zaloga.Text = "Załoga : " + handlarz.Zaloga + " ludzi";
            lbl_kadlub.Text = "Stan kadłuba : " + handlarz.Kadlub + "%";
            lbl_runda.Text = "Runda : " + handlarz.Runda;

            lbl_radioaktivum.Text = "Radioaktivum : " + handlarz.Radioaktivum;
            lbl_krysztal.Text = "Kryształ : " + handlarz.Krysztal;
            lbl_deuter.Text = "Deuter : " + handlarz.Deuter;
            lbl_antymateria.Text = "Antymateria : " + handlarz.Antymateria;

            
        }

        /// <summary>
        /// Stworzenie nowego sklepu + aktualizacja etykiet
        /// </summary>
        public void Stworz_sklep() 
        {
            // Stworzenie sklepu ponieważ przy podróżny na kolejną stacje za każdym razem jest stworzony nowy sklep z nowymi cenami. 
            // Łączenie wyświetlanego tekstu na etykiecie + pobranie wartości z zmiennej
            sklep = new Sklep();
            rb_paliwo.Text = "Paliwo : " + sklep.Paliwo;
            rb_zywnosc.Text = "Żywność : " + sklep.Zywnosc;
            rb_zaloga.Text = "Nowy człowiek : " + sklep.Zaloga;
            rb_kadlub.Text = "Naprawa : " + sklep.Kadlub;

            rb_radioaktivum.Text = "RadioAktivum : " + sklep.Radioaktivum_cena;
            rb_krysztal.Text = "Kryształ : " + sklep.Krysztal_cena;
            rb_deuter.Text = "Deuter : " + sklep.Deuter_cena;
            rb_antymateria.Text = "Antymateria : " + sklep.Antymateria_cena;
        }

        /// <summary>
        /// Co się dzieje w momencie załadowania się okienka
        /// </summary>
        public void Statek_Load(object sender, EventArgs e)
        {
            // Stworzenie Menu do wpisania nazwy gracza i wejścia do gry
            Menu menu = new Menu();
            // Stworzenie Handlarza z nazwą "X" które potem jest zamienione na podane w formularzu MENU
            handlarz = new Handlarz("X");

            // Wywołanie MENU
            menu.ShowDialog();
            // Przypisanie nazwy graczowi
            handlarz.Nazwa = menu.Nazwa_gracza;

            // Wywołanie metod 
            Aktualizuj_handlarza();
            Stworz_sklep();
        }

        #endregion



        #region Metody publiczne

        /// <summary>
        /// Wiadomość MessageBox w wypadku przegranej
        /// </summary>
        public void przegrana_wiadomosc()
        {
            // Wywołanie message box
            string w1 = "Przegrałeś ";
            string w2 = "Statek";  
            MessageBoxButtons przycisk = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(w1, w2, przycisk);
            if (result == DialogResult.OK)
            {
                Environment.Exit(1); // zamyka wszystko
            }
        }

        /// <summary>
        /// Sprawdzenie czy gracz przegrał
        /// </summary>
        public bool przegrana()
        {
            bool przegrana = false;
            // Jeżeli Gracz nie ma skąd skąd wziąć extra pieniędzy
            if(handlarz.Radioaktivum < 1 &&
               handlarz.Krysztal < 1 &&
               handlarz.Deuter <1 &&
               handlarz.Antymateria < 1)
            {
                // Jeżeli Gracza nie stać na paliwo żeby lecieć dalej
                if (handlarz.Paliwo < 200 &&
                    handlarz.Portfel < sklep.Paliwo)
                {
                    przegrana = true;
                }
                // Jeżeli Gracz zniszczył statek
                else if(handlarz.Kadlub < 0)
                {
                    przegrana = true;
                }
                // Jeżeli nie wystarczy mu żywności na podróż
                else if(handlarz.Zywnosc < 2*handlarz.Zaloga &&
                        handlarz.Portfel < sklep.Zywnosc)
                {
                    przegrana = true;
                }
            }
            return przegrana;
        }

        /// <summary>
        /// Wiadomość Message Box jeżeli gracz będzie próbował sprzedać paliwo, żywność, załogę albo kadłub
        /// Nie można tego zrobić
        /// </summary>
        public DialogResult nie_sprzedaż()
        {
            // Wywołanie message box
            string w1 = "Produkt nie jest na sprzedaż";
            string w2 = "SKLEP";
            MessageBoxButtons przycisk = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(w1, w2, przycisk);
            
            return result;
        }

        /// <summary>
        /// Wiadomość Message Box jeżeli gracz będzie próbował sprzedać coś czego nie ma
        /// </summary>
        public DialogResult puste_zasoby()
        {
            // Wywołanie message box
            string w1 = "Nie masz co sprzedawać";
            string w2 = "SKLEP";
            MessageBoxButtons przycisk = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(w1, w2, przycisk);

            return result;
        } 

        /// <summary>
        /// Wiadomość Message Box jeżeli gracz będzie miał niewystarczającą ilośc pieniędzy do zakupu
        /// </summary>
        public DialogResult pusty_portfel()
        {
            //Wywołanie Message Box
            string w1 = "Nie masz więcej pieniędzy";
            string w2 = "Stan portfela";
            MessageBoxButtons przycisk = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(w1, w2, przycisk);


            return result;
        }

        /// <summary>
        /// Zdarzenie losowe które może się wydarzyć podczas podróży do innej stacji
        /// </summary>
        public int losowe_zdarzenie()
        {
            // Losowa wartość
            int losowe = rnd.Next(1, 20);

            // Jeżeli losowa wartość = 1,2,3,4 - to zdarzenie losowe
            // Switch na zmiennej 
            switch (losowe)
            {
                // Case "..." w zależnosci co wypadnie,
                // losowe_zdarzenie_wiadomość MessageBox od "..." 
                // I co w wypadku zdarzenia

                case 1:
                    //losowe_zdarzenie_wiadomosc(1);
                    handlarz.Kadlub -= rnd.Next(20, 50);
                    break;
                case 2:
                    //losowe_zdarzenie_wiadomosc(2);
                    handlarz.Zaloga -= 1;
                    break;
                case 3:
                    //losowe_zdarzenie_wiadomosc(3);
                    handlarz.Zywnosc -= 20;
                    break;
                case 4:
                    //losowe_zdarzenie_wiadomosc(4);
                    handlarz.Paliwo -= 100;
                    break;
                default:
                    //losowe_zdarzenie_wiadomosc(0);
                    break;
                    
            }
            return losowe;
        }

        /// <summary>
        /// Wiadomość Message Box do zdarzenia losowego
        /// </summary>
        /// <param name="losowe"> Parametr pobierający wystąpienie zdarzenia losowego </param>
        public void losowe_zdarzenie_wiadomosc(int losowe)
        {
            // W zależności od zmiennej - wiadomość
            switch (losowe)
            {
                case 1:
                    string w1a = "Zostałeś trafiony przez meteoryt. Twój stan kadłuba został naruszony. " +
                        "Napraw sie w najbliższej stacji";
                    string w1b = "Podróż";
                    MessageBoxButtons przycisk1 = MessageBoxButtons.OK;
                    DialogResult result1;
                    result1 = MessageBox.Show(w1a, w1b, przycisk1);
                    break;
                case 2:
                    string w2a = "Zostałeś napadnięty przez kosmicznych bandytów. " +
                        "Jeden z członków Twojej załogi został zabity";
                    string w2b = "Podróż";
                    MessageBoxButtons przycisk2 = MessageBoxButtons.OK;
                    DialogResult result2;
                    result2 = MessageBox.Show(w2a, w2b, przycisk2);
                    break;
                case 3:
                    string w3a = "Zostałeś napadnięty przez kosmicznych bandytów. " +
                        "Zostałeś okradziony z zapasów, nikomu nic się nie stało";
                    string w3b = "Podróż";
                    MessageBoxButtons przycisk3 = MessageBoxButtons.OK;
                    DialogResult result3;
                    result3 = MessageBox.Show(w3a, w3b, przycisk3);
                    break;
                case 4:
                    string w4a = "Zgubiłeś sie podczas podróży, nadrobiłeś drogi. ";
                    string w4b = "Podróż";
                    MessageBoxButtons przycisk4 = MessageBoxButtons.OK;
                    DialogResult result4;
                    result4 = MessageBox.Show(w4a, w4b, przycisk4);
                    break;
                default:
                    string w5a = "Podróż przebiegła spokojnie.";
                    string w5b = "Podróż";
                    MessageBoxButtons przycisk5 = MessageBoxButtons.OK;
                    DialogResult result5;
                    result5 = MessageBox.Show(w5a, w5b, przycisk5);
                    break;
            }
        }
        #endregion

        #region Metody prywatne

        /// <summary>
        /// Przycisk do lotu na kolejną stacje 
        /// </summary>
        private void button_runda_Click(object sender, EventArgs e)
        {
            // Sprawdzenie czy gracz przegrał

            bool przegrana_pomocnicza = przegrana();
            if (przegrana_pomocnicza)
            {
                przegrana_wiadomosc();
            }



            // Jeżeli gracz nie ma paliwa nie poleci
            if(handlarz.Paliwo < 200)
            {
                // Message Box 
                string w1 = "Nie możesz leciec dalej, nie masz paliwa ";
                string w2 = "Statek";
                MessageBoxButtons przycisk = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(w1, w2, przycisk);
            }
            // Jak gracz nie ma żywności nie poleci
            else if (handlarz.Zywnosc < 2 * handlarz.Zaloga) 
            {
                // Message Box 
                string w1 = "Nie możesz leciec dalej, nie masz jedzenia ";
                string w2 = "Statek";
                MessageBoxButtons przycisk = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(w1, w2, przycisk);
            }
            // Jak nie gracz ma za mało członków załogi nie poleci
            else if (handlarz.Zaloga < 3) 
            {
                // Message Box 
                string w1 = "Nie możesz leciec dalej, nie masz załogi ";
                string w2 = "Statek";
                MessageBoxButtons przycisk = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(w1, w2, przycisk);
            }
            else
            {
                // Statek leci na kolejną stacje

                // Pobiera paliwo za podróż od 100 do 200
                handlarz.Paliwo -= rnd.Next(100, 200);
                // Załoga musi jeść
                handlarz.Zywnosc -= rnd.Next(1, 2) * handlarz.Zaloga; 

                // Może wydarzyć sie zdarzenie losowe
                
                int losowe =losowe_zdarzenie();
                losowe_zdarzenie_wiadomosc(losowe);
                // Sprawdzenie gracz przegrał
                if (przegrana_pomocnicza)
                {
                    przegrana_wiadomosc();
                }

                // Zliczenie kolejnej rundy
                handlarz.Runda++;
                lbl_runda.Text = "Runda : " + handlarz.Runda;

                // Aktualizacja kontrolek i nowy sklep bo nowa stacja
                Stworz_sklep();
                Aktualizuj_handlarza();
            }
            
        }

        /// <summary>
        /// Przycisk do kupowania w sklepie
        /// </summary>
        private void button_kup_Click(object sender, EventArgs e) 
        {
            /*
             *  Jeżeli zaznaczona jest dana kontrolka i spełniony jest warunek :
             *  Ilość pieniedzy musi być większa od ceny
             * 
             *  Gracz nabywa produkt w danej ilości
             *  Odejmowana jest od portfela wartość cenowa produktu
             *  
             * 
            */

            #region Radiobutton KUP

            // Kup Paliwo // 
            if (rb_paliwo.Checked && handlarz.Portfel - sklep.Paliwo > 0)
            {
                  handlarz.Paliwo += 300;
                  handlarz.Portfel -= sklep.Paliwo;
                  Aktualizuj_handlarza();
            }
            else if (rb_paliwo.Checked)
            {
                pusty_portfel();
            }
            // Kup Żywność // 
            if (rb_zywnosc.Checked && handlarz.Portfel - sklep.Zywnosc > 0)
            {
                handlarz.Zywnosc += 10;
                handlarz.Portfel -= sklep.Zywnosc;
                Aktualizuj_handlarza();
            }
            else if (rb_zywnosc.Checked)
            {
                pusty_portfel();
            }
            // Kup Ludzi // 
            if (rb_zaloga.Checked && handlarz.Portfel - sklep.Zaloga > 0)
            {
                handlarz.Zaloga += 1;
                handlarz.Portfel -= sklep.Zaloga;
                Aktualizuj_handlarza();
            }
            else if (rb_zaloga.Checked)
            {
                pusty_portfel();
            }
            // Napraw statek // 
            if ((rb_kadlub.Checked && handlarz.Kadlub < 100) && handlarz.Portfel - sklep.Kadlub > 0)
            {
                handlarz.Kadlub = 100;
                handlarz.Portfel -= sklep.Kadlub;
                Aktualizuj_handlarza();
            }
            else if (rb_kadlub.Checked)
            {
                pusty_portfel();
            }
            
            // KUP/SPRZEDAJ
            // Kup Radioaktivum // 
            if (rb_radioaktivum.Checked && handlarz.Portfel - sklep.Radioaktivum_cena > 0)
            {
                handlarz.Radioaktivum += 1;
                handlarz.Portfel -= sklep.Radioaktivum_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_radioaktivum.Checked)
            {
                pusty_portfel();
            }
            // Kup Kryształ // 
            if (rb_krysztal.Checked && handlarz.Portfel - sklep.Krysztal_cena > 0)
            {
                handlarz.Krysztal += 1;
                handlarz.Portfel -= sklep.Krysztal_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_krysztal.Checked)
            {
                pusty_portfel();
            }
            // Kup Deuter // 
            if (rb_deuter.Checked && handlarz.Portfel - sklep.Deuter_cena > 0)
            {
                handlarz.Deuter += 1;
                handlarz.Portfel -= sklep.Deuter_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_deuter.Checked)
            {
                pusty_portfel();
            }
            // Kup Antymaterie // 
            if (rb_antymateria.Checked && handlarz.Portfel - sklep.Antymateria_cena > 0)
            {
                handlarz.Antymateria += 1;
                handlarz.Portfel -= sklep.Antymateria_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_antymateria.Checked)
            {
                pusty_portfel();
            }
            #endregion
        }

        /// <summary>
        /// Przycisk do sprzedawania surowców 
        /// </summary>
        private void button_sprzedaj_Click(object sender, EventArgs e) // Przycisk do sprzedawania 
        {
            /*
             *  Jeżeli gracz próbuje sprzedać paliwo, żywność, ludzi albo kadłub
             *  Pokazuje się message box że nie może tego zrobić
             *  
             *  Jeżeli spełniony jest warunek że gracz jest w posiadaniu produktu może go sprzedać
             *  Usuwana jest z posiadania i doliczane są środku po kursie sklepu
            */

            // Paliwo
            if (rb_paliwo.Checked)
            {
                nie_sprzedaż();
            }
            // Żywność
            if (rb_zywnosc.Checked)
            {
                nie_sprzedaż();
            }
            // Ludzie
            if (rb_zaloga.Checked)
            {
                nie_sprzedaż();
            }
            // Kadłub
            if (rb_kadlub.Checked)
            {
                nie_sprzedaż();
            }
            // Sprzedaż Radioaktivum
            if (rb_radioaktivum.Checked && handlarz.Radioaktivum > 0)
            {
                handlarz.Radioaktivum -= 1;
                handlarz.Portfel += sklep.Radioaktivum_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_radioaktivum.Checked)
            {
                puste_zasoby();
            }
            // Sprzedaż Kryształu
            if (rb_krysztal.Checked && handlarz.Krysztal > 0)
            {
                handlarz.Krysztal -= 1;
                handlarz.Portfel += sklep.Krysztal_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_krysztal.Checked)
            {
                puste_zasoby();
            }
            // Sprzedaż Deuteru
            if (rb_deuter.Checked && handlarz.Deuter > 0)
            {
                handlarz.Deuter -= 1;
                handlarz.Portfel += sklep.Deuter_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_deuter.Checked)
            {
                puste_zasoby();
            }
            // Sprzedaż Deuteru
            if (rb_antymateria.Checked && handlarz.Antymateria > 0)
            {
                handlarz.Antymateria -= 1;
                handlarz.Portfel += sklep.Antymateria_cena;
                Aktualizuj_handlarza();
            }
            else if (rb_antymateria.Checked)
            {
                puste_zasoby();
            }
        }


        #endregion


    }
}
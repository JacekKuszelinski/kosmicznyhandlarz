﻿/*
 *\file Sklep.cs
 *\author Jacek Kuszeliński, Kevin Łebko
 *\date 29.06.2018
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Kosmiczny_handlarz.Handlarz;


namespace Kosmiczny_handlarzTests
{
    /// <summary>
    /// Testy jednostkowe klasy Handlarz
    /// </summary>
    [TestClass]
    public class HandarzTest
    {
        /// <summary>
        /// Test konstruktora
        /// Brak metod w klasie
        /// </summary>
        [TestMethod]
        public void Handlarz_konstruktorTest()
        {
            //arrange
            Kosmiczny_handlarz.Handlarz handlarzTest = new Kosmiczny_handlarz.Handlarz("Kewin");

            //act
            string nameTest = handlarzTest.Nazwa;
            int paliwoTest = handlarzTest.Paliwo; 

            //arrange
                // Test ustaienia nazwy
            Assert.AreEqual("Kewin", nameTest);
                // Test ustawienia zmiennej paliwo
            Assert.AreEqual(400, paliwoTest);
            
        }
    }
}

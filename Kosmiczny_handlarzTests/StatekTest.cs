﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Kosmiczny_handlarz.Statek;
using static Kosmiczny_handlarz.Sklep;
using static Kosmiczny_handlarz.Handlarz;
using Kosmiczny_handlarz;
using System.Windows.Forms;

namespace Kosmiczny_handlarzTests
{
    [TestClass]
    public class StatekTest
    {
        Kosmiczny_handlarz.Statek StatekTestowy = new Kosmiczny_handlarz.Statek();
        Kosmiczny_handlarz.Handlarz Handlarz = new Kosmiczny_handlarz.Handlarz("roman");
        
        [TestMethod]
        public void Zdarzenie_losoweTest()
        {
            //arrange
            int losowe = StatekTestowy.losowe_zdarzenie();
            bool losoweTest = false;
            //act 
            if (losowe >= 1 && losowe <= 20)
            {
                losoweTest = true;
            }
            //assert
            Assert.AreEqual(true, losoweTest);
            
        }

        /*[TestMethod]
        public void PrzegranaTest()
        {
            //arrange
            
            //act   
            bool przegrana_test = StatekTestowy.przegrana();

            //assert
            Assert.AreEqual(true, przegrana_test);
        }*/
        

        [TestMethod]
        public void BoxTest()
        {
            int sprawdz = 0;
            DialogResult result = StatekTestowy.nie_sprzedaż();
            if (result == DialogResult.OK)
            {
                sprawdz = 1;
            }
            Assert.AreEqual(1, sprawdz);
        }
        [TestMethod]
        public void BoxTest2()
        {
            int sprawdz = 0;
            DialogResult result = StatekTestowy.puste_zasoby();
            if (result == DialogResult.OK)
            {
                sprawdz = 1;
            }
            Assert.AreEqual(1, sprawdz);
        }
        [TestMethod]
        public void BoxTest3()
        {
            int sprawdz = 0;
            DialogResult result = StatekTestowy.pusty_portfel();
            if (result == DialogResult.OK)
            {
                sprawdz = 1;
            }
            Assert.AreEqual(1, sprawdz);
        }
    }
}

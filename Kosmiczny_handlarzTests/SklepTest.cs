﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Kosmiczny_handlarz.Sklep;

namespace Kosmiczny_handlarzTests
{
    [TestClass]
    public class SklepTest
    {
        [TestMethod]
        public void Sklep_konsturuktorTest()
        {
            //arrange
            Kosmiczny_handlarz.Sklep sklepTest = new Kosmiczny_handlarz.Sklep();
            //act
            int paliwoTest = sklepTest.Paliwo;
            int paliwo = 0;
            if (paliwoTest > 100 && paliwoTest < 200) // Sprawdzenie przedziału paliwa
            {
                paliwo = 1;
            }
            //assert
            Assert.AreEqual(1, paliwo);
        }
    }
}
